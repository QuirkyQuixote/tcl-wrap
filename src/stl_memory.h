
#ifndef TCL_WRAP_STL_MEMORY_H_
#define TCL_WRAP_STL_MEMORY_H_

#include <memory>

#include "tcl_wrap.h"

namespace tcl_wrap {

template<class T> struct Traits<std::shared_ptr<T>> {
        static std::string name()
        { return "std::shared_ptr<" + Traits<T>::name() + ">"; }
        static std::string emit(const std::shared_ptr<T>& x)
        { return Traits<T>::emit(*x); }
        template<class S> static std::shared_ptr<T> parse(const S& s)
        { return std::make_shared<T>(Traits<T>::parse(s)); }
};

template<class T> struct Traits<std::weak_ptr<T>> {
        static std::string name()
        { return "std::weak_ptr<" + Traits<T>::name() + ">"; }
        static std::string emit(const std::weak_ptr<T>& x)
        { return Traits<T>::emit(*x); }
        template<class S> static std::weak_ptr<T> parse(const S& s)
        { throw std::runtime_error{"can't make " + name() + " from string"}; }
};

}; // namespace tcl_wrap

#endif  // TCL_WRAP_STL_MEMORY_H_
