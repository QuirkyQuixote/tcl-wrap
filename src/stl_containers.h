
#ifndef TCL_WRAP_STL_CONTAINERS_H_
#define TCL_WRAP_STL_CONTAINERS_H_

#include <vector>
#include <deque>
#include <list>
#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include "tcl_wrap.h"
#include "stl_utils.h"

namespace tcl_wrap {

template<class Cont> std::string emit_container(const Cont& cont)
{
        using T = typename Cont::value_type;
        std::string s;
        bool is_first{true};
        for (auto& x : cont) {
                if (is_first)
                        is_first = false;
                else
                        s += ' ';
                s += enclose(Traits<T>::emit(x));
        }
        return s;
}

template<class Cont, class S> Cont parse_container(const S& s)
{
        using T = typename Cont::value_type;
        std::vector<std::string_view> tokens;
        tokenize(s, std::back_inserter(tokens));
        Cont cont;
        for (auto& x : tokens)
                cont.insert(cont.end(), std::move(Traits<T>::parse(x)));
        return cont;
}

template<class T, class Allocator>
struct Traits<std::vector<T, Allocator>> {
        using Cont = std::vector<T, Allocator>;
        static std::string name()
        { return "std::vector<" + Traits<T>::name() + ">"; }
        static std::string emit(const Cont& x)
        { return emit_container(x); }
        template<class S> static Cont parse(const S& s)
        { return parse_container<Cont>(s); }
};

template<class T, class Allocator>
struct Traits<std::deque<T, Allocator>> {
        using Cont = std::deque<T, Allocator>;
        static std::string name()
        { return "std::deque<" + Traits<T>::name() + ">"; }
        static std::string emit(const Cont& x)
        { return emit_container(x); }
        template<class S> static Cont parse(const S& s)
        { return parse_container<Cont>(s); }
};

template<class T, class Allocator>
struct Traits<std::list<T, Allocator>> {
        using Cont = std::list<T, Allocator>;
        static std::string name()
        { return "std::list<" + Traits<T>::name() + ">"; }
        static std::string emit(const Cont& x)
        { return emit_container(x); }
        template<class S> static Cont parse(const S& s)
        { return parse_container<Cont>(s); }
};

template<class T, class Compare, class Allocator>
struct Traits<std::set<T, Compare, Allocator>> {
        using Cont = std::set<T, Compare, Allocator>;
        static std::string name()
        { return "std::set<" + Traits<T>::name() + ">"; }
        static std::string emit(const Cont& x)
        { return emit_container(x); }
        template<class S> static Cont parse(const S& s)
        { return parse_container<Cont>(s); }
};

template<class T, class Compare, class Allocator>
struct Traits<std::multiset<T, Compare, Allocator>> {
        using Cont = std::multiset<T, Compare, Allocator>;
        static std::string name()
        { return "std::multiset<" + Traits<T>::name() + ">"; }
        static std::string emit(const Cont& x)
        { return emit_container(x); }
        template<class S> static Cont parse(const S& s)
        { return parse_container<Cont>(s); }
};

template<class T, class Compare, class Allocator>
struct Traits<std::unordered_set<T, Compare, Allocator>> {
        using Cont = std::unordered_set<T, Compare, Allocator>;
        static std::string name()
        { return "std::unordered_set<" + Traits<T>::name() + ">"; }
        static std::string emit(const Cont& x)
        { return emit_container(x); }
        template<class S> static Cont parse(const S& s)
        { return parse_container<Cont>(s); }
};

template<class T, class Compare, class Allocator>
struct Traits<std::unordered_multiset<T, Compare, Allocator>> {
        using Cont = std::unordered_multiset<T, Compare, Allocator>;
        static std::string name()
        { return "std::unordered_multiset<" + Traits<T>::name() + ">"; }
        static std::string emit(const Cont& x)
        { return emit_container(x); }
        template<class S> static Cont parse(const S& s)
        { return parse_container<Cont>(s); }
};

template<class K, class V, class Compare, class Allocator>
struct Traits<std::map<K, V, Compare, Allocator>> {
        using Cont = std::map<K, V, Compare, Allocator>;
        static std::string name()
        { return "std::map<" + Traits<K>::name() + "," + Traits<V>::name() + ">"; }
        static std::string emit(const Cont& x)
        { return emit_container(x); }
        template<class S> static Cont parse(const S& s)
        { return parse_container<Cont>(s); }
};

template<class K, class V, class Compare, class Allocator>
struct Traits<std::multimap<K, V, Compare, Allocator>> {
        using Cont = std::multimap<K, V, Compare, Allocator>;
        static std::string name()
        { return "std::multimap<" + Traits<K>::name() + "," + Traits<V>::name() + ">"; }
        static std::string emit(const Cont& x)
        { return emit_container(x); }
        template<class S> static Cont parse(const S& s)
        { return parse_container<Cont>(s); }
};

template<class K, class V, class Compare, class Allocator>
struct Traits<std::unordered_map<K, V, Compare, Allocator>> {
        using Cont = std::unordered_map<K, V, Compare, Allocator>;
        static std::string name()
        { return "std::unordered_map<" + Traits<K>::name() + "," + Traits<V>::name() + ">"; }
        static std::string emit(const Cont& x)
        { return emit_container(x); }
        template<class S> static Cont parse(const S& s)
        { return parse_container<Cont>(s); }
};

template<class K, class V, class Compare, class Allocator>
struct Traits<std::unordered_multimap<K, V, Compare, Allocator>> {
        using Cont = std::unordered_multimap<K, V, Compare, Allocator>;
        static std::string name()
        { return "std::unordered_multimap<" + Traits<K>::name() + "," + Traits<V>::name() + ">"; }
        static std::string emit(const Cont& x)
        { return emit_container(x); }
        template<class S> static Cont parse(const S& s)
        { return parse_container<Cont>(s); }
};

}; // namespace tcl_wrap

#endif  // TCL_WRAP_STL_CONTAINERS_H_
