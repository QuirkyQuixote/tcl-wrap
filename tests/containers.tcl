
load ./libtest.so test

proc test {cont} {
        puts "=== $cont ==="
        try { puts [typeof [${cont}::make]] } on error e { puts $e }
        try { puts [${cont}::peek ""] } on error e { puts $e }
        try { puts [${cont}::peek "foo"] } on error e { puts $e }
        try { puts [${cont}::peek "foo bar"] } on error e { puts $e }
        try { puts [${cont}::peek "foo bar baz"] } on error e { puts $e }
        try { puts [${cont}::peek "foo foo foo"] } on error e { puts $e }
        try { puts [${cont}::peek "{foo 1}"] } on error e { puts $e }
        try { puts [${cont}::peek "{foo 1} {bar 2}"] } on error e { puts $e }
        try { puts [${cont}::peek "{foo 1} {bar 2} {baz 3}"] } on error e { puts $e }
        try { puts [${cont}::peek "{foo 1} {foo 2} {foo 3}"] } on error e { puts $e }
}

test std::vector
test std::deque
test std::list
test std::set
test std::mset
test std::uset
test std::umset
test std::map
test std::mmap
test std::umap
test std::ummap

