
load ./libtest.so test

proc test {cont} {
        puts "=== $cont ==="
        try { puts [typeof [${cont}::make]] } on error e { puts $e }
        try { puts [${cont}::peek ""] } on error e { puts $e }
        try { puts [${cont}::peek "foo"] } on error e { puts $e }
        try { puts [${cont}::peek "foo bar"] } on error e { puts $e }
        try { puts [${cont}::peek "foo bar baz"] } on error e { puts $e }
        try { puts [${cont}::peek "foo bar baz qux"] } on error e { puts $e }
}

test std::pair
test std::tuple1
test std::tuple2
test std::tuple3
test std::array1
test std::array2
test std::array3
