
#include <iostream>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "tcl.h"
#include "readline/readline.h"
#include "readline/history.h"

// The test application has three modes:

// If some arguments are provided, assume them to be file paths, and call
// Tcl_EvalFile() for each one.

// If stdin is a terminal, enter interactive mode, using readline to read
// commands and Tcl_Eval() to evaluate each one.

// Otherwise just read lines from stdin and pass them to Tcl_Eval() until
// something goes wrong.

void load_files(Tcl_Interp* interp, int argc, char* argv[])
{
        for (int i = 1; i < argc; ++i) {
                Tcl_EvalFile(interp, argv[i]);
                auto result = Tcl_GetStringResult(interp);
                if (result && *result)
                        std::cout << result << "\n";
        }
}

void interactive(Tcl_Interp* interp)
{
        using_history();
        for (;;) {
                auto line = readline(">> ");
                Tcl_Eval(interp, line);
                auto result = Tcl_GetStringResult(interp);
                if (result && *result)
                        std::cout << result << "\n";
                add_history(line);
                free(line);
        }
}

void load_stdin(Tcl_Interp* interp)
{
        std::string line;
        while (std::cin.eof() == false) {
                std::getline(std::cin, line);
                Tcl_Eval(interp, line.data());
                auto result = Tcl_GetStringResult(interp);
                if (result && *result)
                        std::cout << result << "\n";
        }

}

int main(int argc, char* argv[])
{
        Tcl_Interp* interp = Tcl_CreateInterp();
        if (argc > 1)
                load_files(interp, argc, argv);
        else if (isatty(fileno(stdin)))
                interactive(interp);
        else
                load_stdin(interp);
        Tcl_DeleteInterp(interp);
        return 0;
}
